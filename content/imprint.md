---
title: "Imprint"
date: 2022-06-02T08:53:42+02:00
draft: false
---


The following provides mandatory data concerning the provider of this website, obligations with regard to data protection, as well as other important legal references involving this webpage as required by German law.

## Provider
The provider of this Internet site within the legal meaning of the term is the Max Planck Institut of Molecular Physiology.

## Address
Max Planck Institute of Molecular Physiology  
Otto-Hahn-Str. 11  
D-44227 Dortmund  
Phone: +49 (231) 133 - 2522

## Editor/Technically responsible
Dr. Johann Jarzombek  
johann.jarzombek@mpi-dortmund.mpg.dev
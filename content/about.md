---
title: Synopsis
featured_image: "img/featured.png"
omit_header_text: false
description: Biomecanet's Aims
type: page
menu: main

---

BIOMECANET aims to reconstitute crucial aspects of [cell division](https://en.wikipedia.org/wiki/Cell_division) in vitro with [purified components](https://en.wikipedia.org/wiki/Synthetic_biology) and realistically [simulate these processes in the computer](https://cytosim.org). 

![sim](/img/sim_3D_bundle.png)
[movie](https://www.youtube.com/watch?v=2Mn9wEv6I2M&t=44s)

The emphasis will be on how [the cell cycle](https://en.wikipedia.org/wiki/Cell_cycle) influences the mechanical workings of [the cytoskeleton](https://en.wikipedia.org/wiki/Cytoskeleton) and how the physicochemical monitoring of the [chromosome](https://en.wikipedia.org/wiki/Chromosome) [bi-orientation](https://en.wikipedia.org/wiki/Spindle_apparatus) process affects the cell cycle. 

With an official start date in July 2021, BIOMECANET will be funded for a period of 6 years by the [European Research Council](https://en.wikipedia.org/wiki/European_Research_Council) (ERC) under the [Synergy Grant scheme](https://erc.europa.eu/funding/synergy-grants). A structured, continuous collaboration of the computational and biochemical groups will inspire the work through the duration of funding.


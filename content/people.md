---
title: People
featured_image: "img/featured.png"
omit_header_text: false
description: Biomecanet's Groups
type: page
menu: main

---


BIOMECANET is a collaboration of the computational simulation group of [François Nédélec (Sainsbury Laboratory, University of Cambridge, UK)](https://www.slcu.cam.ac.uk/people/francois-nedelec) with the experimental working groups of [Andrea Musacchio (MPI of Molecular Physiology, Dortmund, DE)](https://www.mpi-dortmund.mpg.de/research-groups/musacchio) and [Thomas Surrey (CRG, Barcelona, SP)](https://www.crg.eu/ca/programmes-groups/surrey-lab).

![PIs](/img/thomas_andrea_francois.png)  

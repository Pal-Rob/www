---
title: "Henkin et al. 2022"
date: 2022-06-01T10:09:23+02:00
draft: false
featured_image: "/img/henkin2022.jpg"
---

# Crosslinker design determines microtubule network organization by opposing motors


During cell division, the spindle apparatus segregates duplicated chromosomes for their inheritance by the daughter cells. The spindle is a highly interconnected network of microtubule filaments that are crosslinked by different types of molecular motors. How the different motors cooperate to organize the spindle network is not understood. Here, we show that an asymmetric crosslinker design can confer bifunctionality to a mitotic motor in the presence of other motors. The asymmetric motor supports both extensile and contractile microtubule network behaviors as observed in different parts of the spindle. These findings define new rules controlling the generation of active microtubule networks and allow us to better understand how motors cooperate to organize the correct spindle architecture when a cell divides.

Full text available on BioRxiv:

[Crosslinker design determines microtubule network organization by opposing motors](https://www.biorxiv.org/content/10.1101/2022.04.22.488007v1?rss=1)

Gil Henkin, Wei-Xiang Chew, François Nédélec and Thomas Surrey

